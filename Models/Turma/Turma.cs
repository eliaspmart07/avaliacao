﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Avaliacao.Models.Turma
{
    public class Turma
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public int Capacidade { get; set; }
        public int EscolaId { get; set; }
        public Escola.Escola Escola { get; set; }
        public List<Aluno.Aluno> Alunos { get; set; }

    }
}
