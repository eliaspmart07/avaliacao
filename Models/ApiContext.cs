﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Avaliacao.Models
{
    public class ApiContext : DbContext 
    {
        public ApiContext(DbContextOptions<ApiContext> options)
          : base(options)
        { }
        public DbSet<Aluno.Aluno> Alunos { get; set; }
        public DbSet<Turma.Turma> Turmas { get; set; }
        public DbSet<Escola.Escola> Escolas { get; set; }
    }
}
