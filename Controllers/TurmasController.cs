﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Avaliacao.Models;
using Avaliacao.Models.Turma;

namespace Avaliacao.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TurmasController : ControllerBase
    {
        private readonly ApiContext _context;

        public TurmasController(ApiContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Turma>>> Listar()
        {
            var turmas = await _context.Turmas.ToListAsync();
            turmas.ForEach(t => t.Escola = _context.Escolas.FirstOrDefault(e => e.Id == t.EscolaId));
            turmas.ForEach(t => t.Alunos = _context.Alunos.Where(a => a.TurmaId == t.Id).ToList());

            var result = turmas.Select(e => new
            {
                Id = e.Id,
                Nome = e.Nome,
                Logradouro = e.Capacidade,
                Complemento = e.EscolaId,
                Escola = e.Escola.Nome,
                Alunos = e.Alunos.Select(a => a.Nome).ToList()
            });

            return Ok(result);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Turma>> GetTurma(int id)
        {
            var turma = await _context.Turmas.FindAsync(id);

            if (turma == null)
            {
                return NotFound();
            }

            turma.Escola = _context.Escolas.FirstOrDefault(e => e.Id == turma.EscolaId);
            turma.Alunos = await _context.Alunos.Where(a => a.TurmaId == turma.Id).ToListAsync();

            var result = new
            {
                Id = turma.Id,
                Nome = turma.Nome,
                Logradouro = turma.Capacidade,
                Complemento = turma.EscolaId,
                Escola = turma.Escola.Nome,
                Alunos = turma.Alunos.Select(a => a.Nome).ToList()
            };

            return Ok(result);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Editar(int id, Turma turma)
        {
            if (id != turma.Id)
            {
                return BadRequest();
            }

            _context.Entry(turma).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TurmaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        [HttpPost]
        public async Task<ActionResult<Turma>> Cadastro(Turma turma)
        {
            _context.Turmas.Add(turma);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetTurma", new { id = turma.Id }, turma);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<Turma>> Excluir(int id)
        {
            var turma = await _context.Turmas.FindAsync(id);
            if (turma == null)
            {
                return NotFound();
            }

            _context.Turmas.Remove(turma);
            await _context.SaveChangesAsync();

            return turma;
        }

        private bool TurmaExists(int id)
        {
            return _context.Turmas.Any(e => e.Id == id);
        }
    }
}
