﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Avaliacao.Models;
using Avaliacao.Models.Escola;

namespace Avaliacao.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EscolasController : ControllerBase
    {
        private readonly ApiContext _context;

        public EscolasController(ApiContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Escola>>> Listar()
        {
            var escolas = await _context.Escolas.ToListAsync();
            escolas.ForEach(e => e.Turmas = _context.Turmas.Where(t => t.EscolaId == e.Id).ToList());

            var result = escolas.Select(e => new 
            { 
                Id = e.Id,
                Nome = e.Nome,
                Logradouro = e.Logradouro,
                Complemento = e.Complemento,
                Bairro = e.Bairro,
                Cidade = e.Cidade,
                Estado = e.Estado,
                Turmas = e.Turmas.Select(t => t.Nome).ToList()
            });

            return Ok(result);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Escola>> GetEscola(int id)
        {
            var escola = await _context.Escolas.FindAsync(id);

            if (escola == null)
            {
                return NotFound();
            }

            escola.Turmas = await _context.Turmas.Where(e => e.EscolaId == escola.Id).ToListAsync();

            var result = new
            {
                Id = escola.Id,
                Nome = escola.Nome,
                Logradouro = escola.Logradouro,
                Complemento = escola.Complemento,
                Bairro = escola.Bairro,
                Cidade = escola.Cidade,
                Estado = escola.Estado,
                Turmas = escola.Turmas.Select(t => t.Nome).ToList()
            };

            return Ok(result);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Editar(int id, Escola escola)
        {
            if (id != escola.Id)
            {
                return BadRequest();
            }

            _context.Entry(escola).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EscolaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        [HttpPost]
        public async Task<ActionResult<Escola>> Cadastro(Escola escola)
        {
            _context.Escolas.Add(escola);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetEscola", new { id = escola.Id }, escola);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<Escola>> Excluir(int id)
        {
            var escola = await _context.Escolas.FindAsync(id);
            if (escola == null)
            {
                return NotFound();
            }

            _context.Escolas.Remove(escola);
            await _context.SaveChangesAsync();

            return escola;
        }

        private bool EscolaExists(int id)
        {
            return _context.Escolas.Any(e => e.Id == id);
        }
    }
}
