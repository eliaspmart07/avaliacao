﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Avaliacao.Models;
using Avaliacao.Models.Aluno;

namespace Avaliacao.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AlunosController : ControllerBase
    {
        private readonly ApiContext _context;

        public AlunosController(ApiContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Aluno>>> Listar()
        {
            var alunos = await _context.Alunos.ToListAsync();
            alunos.ForEach(a => a.Turma = _context.Turmas.FirstOrDefault(t => t.Id == a.TurmaId));

            var result = alunos.Select(a => new
            {
                Nome = a.Nome,
                Id = a.Id,
                DataNascimento = a.DataNascimento,
                TurmaId = a.TurmaId,
                Turma = a.Turma.Nome
            });

            return Ok(result);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Aluno>> GetAluno(int id)
        {
            var aluno = await _context.Alunos.FindAsync(id);

            if (aluno == null)
            {
                return NotFound();
            }

            aluno.Turma = _context.Turmas.FirstOrDefault(t => t.Id == aluno.TurmaId);

            var result = new 
            {
                Nome = aluno.Nome,
                Id = aluno.Id,
                DataNascimento = aluno.DataNascimento,
                TurmaId = aluno.TurmaId,
                Turma = aluno.Turma.Nome
            };

            return Ok(result);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Editar(int id, Aluno aluno)
        {
            if (id != aluno.Id)
            {
                return BadRequest();
            }

            _context.Entry(aluno).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AlunoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        [HttpPost]
        public async Task<ActionResult<Aluno>> Cadastro(Aluno aluno)
        {
            _context.Alunos.Add(aluno);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetAluno", new { id = aluno.Id }, aluno);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<Aluno>> Excluir(int id)
        {
            var aluno = await _context.Alunos.FindAsync(id);
            if (aluno == null)
            {
                return NotFound();
            }

            _context.Alunos.Remove(aluno);
            await _context.SaveChangesAsync();

            return aluno;
        }

        private bool AlunoExists(int id)
        {
            return _context.Alunos.Any(e => e.Id == id);
        }
    }
}
